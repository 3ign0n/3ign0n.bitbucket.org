---
layout: page
title: 
---
{% include JB/setup %}


<div class="posts">
<h2>Recent posts</h2>
<ul>
  {% for post in site.posts limit:5 %}
    <li><span>{{ post.date | date_to_string }}</span>&nbsp;&raquo;&nbsp;<a href="{{ BASE_PATH }}{{ post.url }}">{{ post.title }}</a></li>
  {% endfor %}
</ul>
</div>


