---
layout: post
title: ミニマリスト・ランナーになろう
category : running
tags : [running]
---

{% include JB/setup %}

## ミニマリスト？ベアフット？今までの走り方と何が違うの？

早いもので、日常的に走り始めてから約4年が経ちました。何事も3年ぐらいやると、当初の緊張が解け、表面的な格好や技術から、一歩踏み込んだ世界へ入っていく入り口に立つことができます。
わたしにとって『走る』ということは何なのか、そんなことを思っていた時に出逢った[RUNNING NUTのMinimalist Runningの記事](http://www.runningnut.com/minimalist-running/?fb_action_ids=519530601407335&fb_action_types=og.likes&fb_source=aggregation&fb_aggregation_id=246965925417366)がとても素晴らしい内容なので紹介。


    There is much confusion in the running world between minimalist running and barefoot running. While many minimalist runners are barefoot runners, footwear is just one aspect of a minimalist runner. My idea of minimalist running is different. Instead of being related to the type of footwear we wear, it’s a much broader concept and can be applied to nearly every aspect of running.

    ランニングの世界では、ミニマリストランニングとベアフット(=裸足)ランニングについて混乱があります。多くのミニマリストランナーはベアフットランナーですが、どんな靴を履くのかはミニマリストランナーにとって一側面でしかありません。私のミニマリストランニングに対する考えは異なっています。ミニマリストという考え方は、どんな靴を履くのかではなく、もっと広義の、そして、ランニング全般に適用できるものです。

<a href="http://www.runningnut.com/wp-content/uploads/2010/12/minimalist-runner.jpg"><img src="http://www.runningnut.com/wp-content/uploads/2010/12/minimalist-runner.jpg"></a>
[Image Source](http://www.flickr.com/photos/ncultra/435624463/)

## なぜミニマリストランニング？

    Why try minimalist running?

    Minimalist running can have many benefits, depending on which areas of running you decide to simplify. Not all runners are the same so while you may find benefits adopting the minimalist running concept, another runner may find minimalist running to have a negative effect on their training.

    In addition, incorporating minimalist running into different areas of your training will have different benefits to each one another. For example, taking a minimalist approach to the running clothing you wear will likely lead to saving a significant amount of money. On the other hand, wearing minimalist running shoes or training barefoot will likely lead to a dramatic change in your running style and decreased chances of future injuries.

    ランニングのどの分野をシンプルにするかにもよりますが、ミニマリストランニングには多くの利点があります。ランナーは皆同じではありません。したがって、あなたがミニマリストランニングの考え方を適用することで恩恵を受けたとしても、他のランナーのトレーニングには負の効果が出る場合もあります。

    さらに、ミニマリストランニングをトレーニングの異なる分野に組み込む事で、相互に利益をもたらす場合があります。例えば、ミニマリストのアプローチをランニングウェアに適用した場合、結果としてかなり金銭的な節約になるでしょう。一方で、ミニマリストランニング用の靴を履いたり、裸足でトレーニングすることは、走り方を劇的に変え、怪我をする可能性を減らすことになるでしょう。

わたしも走り始めてから1年半ぐらいはよく怪我をしていました。膝が痛くなったり、マメができたり。走ると健康になると言いますが、靴が合っていなかったり、身体の使い方が間違っているせいで、怪我をするのはよくある話で、いったいどっちなんだよって言うね。

## ミニマリストランニングは何と関係があるのか？

    Minimalist running is about simplifying running in as many different areas as possible. It takes on the approach that less is more and that more enjoyment and results can be gained from making running as simple as possible. Minimalist running involves both the physical aspect of running (shoes, clothing, accessories, technology, running mechanics, training routes) and also the non physical aspects (mental approach, achieving goals, training schedule, data).

    ミニマリストランニングではランニングのさまざまな分野をシンプルにします。これは『より少ないことは、より豊かなこと』というアプローチを取ることであり、可能な限りシンプルに走ることによって、より喜びや成果を得ることです。ミニマリストランニングには、ランニングの物理的な側面（靴、服、アクセサリ、技術、ランニングメカニズム、トレーニングルート）と、非物理的な側面（精神的な姿勢、目標の達成、トレーニング計画、データ）の両方が関係あります。

この『シンプルにする』という考え方にとても共感したので、わたしもミニマリストランニングに興味を持ったのでした。

## 計画的なトレーニングを止めよう
    
    The human body is not a machine and we should not treat it as one with strict training schedules outlining when we can and cannot run. Although there is a need to track workouts and ensure adequate rest between runs, setting out a training schedule months in advance seems a bit ridiculous. Instead, listen to your body and run on days you feel like it and rest when you feel tired or sluggish. In addition, if you feel great on a run then do some extra distance. On days where you just want to stop, cut the workout short and make up for it later in the week.

    人間の身体は機械ではありません。私たちは、厳密なトレーニング計画によって、走ることができる時とできない時を同様に扱うべきではありません。トレーニングを追跡し、ランの間の適切な休養期間を確認することは必要なことですが、1ヶ月のトレーニング計画を前もって立てるというのは非合理的です。自身の身体に耳を傾け、走りたい時に走り、疲れている時やゆっくりしたい時は休みましょう。さらに、調子がいい時は追加の距離を走ってもよいのです。止めたい時は、トレーニングを中断し、後で埋め合わせをしたって良いのです。

これには良い点と悪い点があると思います。良い点は、走ることを純粋に楽しめるということ。悪い点は、レースで記録を伸ばすにはやはりあるい程度走りこまないといけませんが、特に初心者のうちは、怠惰になったり、走らない理由を見つけてサボってしまいがちです。何事も楽しくなるためには、ある程度の苦労は必要だということです。あまり追い込んで辛くなったら一切走るのを止めてしまうことになるので、それも意味はないのですが…私自身が次のレベルのランナーになるには、月間走行距離が200kmを超えるぐらいにならないと、というのが大体の感覚なのですが、夏は50km/month、冬は150km/monthがいいところ。まぁ、この計測の考え方がもうミニマリストな感じではないですね。


## 装飾的なランニングシューズを履くのを止めよう

    Todays running shoes feature all kinds of useless features such as support cushions, gel pockets and other materials to absorb shock. This shoe technology is supposedly meant to decrease injury, but many runners soon find otherwise. Instead, choose a running shoe that allows for the natural movement of your foot rather than restrict this natural motion. Look for a light shoe with a low heel. Vibram Five Fingers are becoming popular with todays minimalist runners.

    今日のランニングシューズには、サポートクッションやジェルポケット、振動吸収素材といった全く役に立たない特徴を持っています。こうした靴のテクノロジーは、怪我を減少させると思われていますが、多くのランナーはその反対だということに気づくでしょう。足の自然な動きを妨げるような靴ではなく、自然な動きを許容するような靴を選びましょう。踵の低い、軽い靴を探しましょう。Vibram Five Fingersはミニマリストランナーたちの間で人気が出てきています。

わたしも[VFF(Vibram Five Fingers、ビブラムファイブフィンガーズ)](http://www.barefootinc.jp/)を買ってしまいました。VFFのレポートや、クッション付きの靴がどうして足に良くないのかの詳しい話は別の機会に書こうと思います。
    

## 靴を脱ぎ捨てよう

    Some minimalist runners only run barefoot. It takes time and patience to get to a stage where you are able to all your training without shoes because your feet muscles need to strengthen and the soles of your feet need to toughen. Instead of running completely barefoot, many minimalist runners train mostly with minimal shoes and do some shorter workouts barefoot. Many of the benefits barefoot running offers can still be achieved by doing this.

    裸足でしか走らないミニマリストランナーもいます。すべてのトレーニングを全く靴を履かないでこなせるようになるには、長い時間と忍耐を伴うでしょう。これには足の筋力と足裏の強靭さが必要になります。完全に裸足になる代わりに、多くのミニマリストランナーは大部分のトレーニングを最小限の靴を履いて行い、短い時間、裸足で行います。こうすることでもベアフットランニングの恩恵の多くを受けることができます。

完全に裸足で歩いたり、走ったりを安全にできるところは少ないですね。よく整備された芝生の公園や、綺麗な砂浜だったら裸足で歩いたり、走ったりできるかもしれません。芝生の上は本当に気持ちが良いので機会があったら是非試してみてください。今後、そういうことができる公園も紹介していきたいと思います。

## ランニングフォームを変えよう

    Minimalist running also involves running in a more natural style. This takes time to develop and can be assisted by doing some of your training barefoot. Your feet should strike directly under your body rather than in front and your center of gravity should be positioned directly under your foot strike. Also, instead of landing on your heels, you should utilize the natural shock absorption of landing on the balls of your feet. By using these two suggestions, you will also find an increase in the cadence of your stride.

    ミニマリストランニングはもっと自然な形で走るということでもあります。これを習得するには時間がかかりますが、裸足でトレーニングすることが助けとなるでしょう。足は前ではなく身体の直下に来るべきで、重心の真下に足の着地点があるべきです。踵で着地するのではなく、土踏まずの部分で着地することによる自然な衝撃吸収を活用すべきです。これら2つの提案を実践するとケイデンスが上がる(歩調が早くなる)ことに気づくでしょう。

わたしはVFFを履いても、ランニングフォームはほとんど変わっていません。元々故障に苦しんだということもあり、いかに身体への衝撃を少なくするか、いかに効率良く走るかということは靴をミニマムなものに変える前から試行錯誤していたので、自然と足裏全体もしくは、前側で着地するようになっていました。

<a href="http://www.runningnut.com/wp-content/uploads/2010/12/running-gear.jpg"><img src="http://www.runningnut.com/wp-content/uploads/2010/12/running-gear.jpg"></a>
[Image Source](http://www.flickr.com/photos/jason_coleman/62317469/)


## ペースを計測するのを止めよう

    While it may be interesting to find out your running pace after completing a marathon or race, there is no need to calculate and measure the pace of every single training run. The only pace you should be interested in is the one that feels most comfortable for the training run you are doing. Some days you will want to run fast and challenge yourself, on others you might just want to run slowly and enjoy yourself.

    マラソンやレースを走り終えた後、ランニングペースを見るのは楽しいかも知れませんが、トレーニングの度にペースを計算したり計測したりする必要はありません。最も興味を持つべきペースは、行なっているトレーニングが快適かどうかだけです。早く走りたい日や、自分に挑戦したい日もあれば、ゆっくり、楽しんでやりたい日もあるでしょう。

走る時は[Runkeeper](http://runkeeper.com/)を使っています。ラップタイムとか、あまり気にして走ってませんが、[Runkeeper](http://runkeeper.com/)の音声キューが1km毎にペース等を読み上げてくれるので、それは聞いています。


## 終了時間を予想するのを止めよう

    Nearly every runner sets specific goals for their training or specific races they enter into. Instead of having a finishing time as a goal, use a goal to simply finish, be happy with your results or even to run as fast as you can on race day. Even though you are not predicting any times, it does not mean you can’t try to get a time as fast as you can.

    ほとんどのランナーがトレーニングの目標やどのレースに参加するを決めています。ゴールする時間を決めるのではなく、単にゴールすることを目的にし、結果を喜び、その日にできる最善を尽くしましょう。時間を予測しなくても最善を尽くせないというわけではありません。

レースの時は、これは元々やってませんでした。もちろん目標タイムはありますが、その日の体調や天候等に左右されるので、当日あがいても仕方のないことです。それに、練習した以上の成果はだせないものです。普段のトレーニングに関しては、大体の時間は決めています。

## 心拍計を使うのを止めよう

    Unless you use a heart rate monitor for a health related reason, stop calculating your heart rate in training runs. Instead of letting a heart rate monitor tell you how fast to run, listen to your body and run at a pace that feels most appropriate for the training run. Heart rate monitors are simply an indicator of how fast your pace is, instead of displaying this in digital form it can be measured by just being conscious of your running pace and how you are feeling during the run.

    健康的な理由から心拍計を使っているのでなければ、トレーニングの際に心拍計を着けるのを止めよう。どのぐらい早く走れるかを知るのに心拍計を使う代わりに、身体に耳を傾け、トレーニングに最も適切と感じるペースで走ろう。心拍計は、走っているペースをどう意識しているかや、走っている間にどういう気分なのかを計測してデジタル化して表示しているわけではなく、単にどのぐらいのペースで走っているかを示しているにすぎない。

心拍計をするようになってから約1年。大体どのぐらいキツイ感じで、どのぐらいの心拍なのかは把握できるようになってきました。レースの時はあると安心、予想以上に前半ペースが上がってるとか、そういうことが冷静に判断できるので、後半バテたとか、そういうのを未然に防ぐことができます。でも、なにも身に付けないほうが気持よく走れるので、最近は自分を追い込む気分ではない時は心拍計は付けないで走るようになりました。


## ブランドもののランニングギアを買うのを止めよう

    The only running gear that needs to be branded is your running shoes. Shirts, hats, shorts, socks and other accessories do not need to be branded or even made out of running specific materials. Apart from very cold conditions, you do not even need any clothes specifically designed for running. Running clothes just make running more comfortable. While its a good idea to buy running clothes, its not really necessary to buy expensive branded items.

    必要なブランドもののランニングギアは、ランニングシューズだけです。シャツ、帽子、ショートパンツ、靴下、その他のアクセサリはブランドものやランニング用に作られたものである必要もありません。とても寒い状況でないなら、ランニング用に特別にデザインされた服は必要ありません。ランニング用の服は、ランニングをより快適にしてくれるだけです。ランニング用の服を買うのは良い考えですが、高価なブランドアイテムを買う必要は全くありません。

これは本当にそう。でも、カッコイイ／カワイイ服で着飾ったら、気分が上がって、気持よく走れるっていうのは、特に初心者の頃はわたしもそうだったなぁ。最近はVFFだから靴下も履かなくなったけど、変な靴下だとマメが出来たり、靴ずれを起こすので気をつけたほうがいいところだしね。あと、わたしはまだミニマリストな習慣もついてないし、テクノロジー信奉者なので、コンプレッション系のウェアはやっぱり効果あると思うのよね。無駄にたくさん買う必要は無いし、ブランド名に躍らされることなく、いいものを見極めて、本物を1つ持ったほうが、単に走るという行為もずっと気持ちがいいものになるはず、と、わたしは思っています。


## サプリメントは必要ない

    Energy gels, protein bars and other sport supplements are not needed for running. Any benefits they offer can be found in natural foods. The only advantage these supplements offer is convenient packaging. Because of this, it might make sense to use them for longer races however for day to day training they are completely unnecessary.

    エナジージェル、プロテインバー、その他のスポートサプリメントは走るのに必要ありません。それらから受けられる恩恵は、自然食品の中にもあります。これらのサプリメントが唯一優れているのは、便利に梱包されているということだけです。このため、長いレースにおいては意味があるかも知れませんが、日々のトレーニングには全く必要無いものです。

わたしも走り始めて最初の頃は、サプリメントを飲んだり、プロテインを摂取したりしてた。今は一切そういうことはせず、食事から必要な栄養を取るようにしている。トレーニング中も、20km未満の場合はエネルギーの補給は一切しない。夏場でなければ、水の補給も基本的にはしない。そうすることで、余計な荷物を持たなくて済むし、ウエストポーチ等を身に付けずに走れるというのは本当に身軽で気分がいいものだよ。


## ランニングアクセサリについて

    Fuel belts, GPS watches and most other running accessories are not needed for running. They only make running more complicated and there are more simple approaches to receiving the same results. However, if you need any accessory for a safety point of view, then disregard this advice. This point is more catered towards the runner that does a short route through the city wearing a fuel belt rather than a runner doing an ultramarathon through and isolated trail run.

    補給用のベルト、GPS付き時計、その他のランニングアクセサリは走るのに必要ありません。これらはランニングを複雑にするだけです。同じ結果を出すのにもっとシンプルなアプローチがあります。安全性の観点からアクセサリが必要なら、この助言は無視してください。ウルトラマラソンや、単独のトレイルランをしているランナーではなく、市街地の短い距離を補給ベルトをつけて走っているランナーに向けての助言です。

後述のランニングルートの話とも関連して、気ままに走っていると迷子になるのでGPSは欠かせないですね。わたしは、トレーニングで走る時は、補給食もお金も一切持たずに出かける事がほとんどなので、自分の体力の残りと、家にたどり着くまでの距離との勝負だし、脇道にもよくそれるので特に夜はGPSないと生きて戻ってこれない身の危険を感じることもあります。


## トレッドミルを使うのを止めよう

    Buying an expensive machine to mimic a movement that can be performed exactly the same by stepping outdoors is not taking a minimalist approach. Buying a treadmill may be acceptable in some circumstances, such as in extremely cold conditions. Heading outdoors and running through a scenic trail run is much more natural and less complicated than relying on a machine for workouts.

    屋外で同じ事ができるのに、それを模した動きをするための高価な機械を買うのはミニマリストのアプローチではありません。極寒状態のような状況では、トレッドミルを購入するのは許容されるかもしれません。トレーニングの為に機械に頼るより、外に出て、眺めのよりトレイルを走る方がずっと自然で、シンプルです。

これは全面的に賛成。走りはじめた頃、ジムのお試し会員になって1ヶ月ぐらいやったけど、それはもうつまらなくて続かなかったよ。屋外でさえ同じ所を何周も走るのが好きではないわたしが、トレッドミルなんて続くわけがありません。

## トレーニング用ルートを使うのを止めよう

    Many runners use the exact same training routes and run them over an over. Not only is this extremely boring, but it’s enforcing a structure on your training which is not really the minimalist approach. Instead, when you step out the door for a workout, have no set running route and run where you feel like running. The only disadvantage of this approach is that you need to be mindful of the distance you run.

    多くのランナーは、全く同じトレーニングルートを何度も何度も使っています。これはとても退屈なだけでなく、全くミニマリストアプローチではないトレーニングの構造を強制してしまっています。トレーニングの為に外に飛び出したら、ランニングルートは設定せず、走りたいと感じるままに走りましょう。このアプローチの欠点は、走っている距離を気にかける必要があるということです。

大枠のルートは決めて、あとはその時の気分で、色々な脇道に入ることはよくあるね。そうして美味しそうなお店とか、綺麗な景色とか、そういうのを発見するのがとても楽しいよね。

<a herf="http://www.runningnut.com/wp-content/uploads/2010/12/minimalist-runner-stretching.jpg"><img src="http://www.runningnut.com/wp-content/uploads/2010/12/minimalist-runner-stretching.jpg"></a>
[Image Source](http://www.flickr.com/photos/lululemonathletica/4312506290/)

## トレーニングを記録しよう

    While extreme minimalist runners may not agree with this point, I still see a strong reason to record how much you train. However, simplify the data as much as possible. Instead of writing everything down about your workout such as the distance ran, the time it took you, the time of the day, what you ate beforehand etc, just choose one metric and look at this alone. Never compare distance to time- choose one and use it to record your training.

    極端なミニマリストランナーは、この点には同意しないでしょうが、私にはどのぐらいトレーニングをしたか記録する強い理由があります。しかし、可能な限りデータは簡略化しています。走った距離、かかった時間、時刻、事前に何を食べたか、といったような、トレーニングの全てを記録するのではなく、1つを選んで記録し、それを単独で見ましょう。距離と時間を比較したりせず、1つを選び、それをトレーニングの記録として使いましょう。

最初、この項は何かの間違いかと思った。a strong reasonも説明されてないし、今までの話の流れからして、全くミニマリスト的アプローチじゃない。でも、わたしも[Runkeeper](http://runkeeper.com/)を使っている。ここでは計測するのは1つの指標だけとの話だけど、まぁ、走り始めにボタンを押して、終わったらもう一回押すだけなんだし。後で分析したりはあまりしてない。


## 音楽を聞きながらトレーニングをするのを止めよう

    Many runners perform training runs or even races while listening to music. Remove this unnecessary aspect of your training and when you train outside, listen to the sound of your footsteps, the sound of birds and other natural sounds. You may be limited by where you live but even when running in a city, there are many interesting sounds to listen to.

    多くのランナーは、トレーニング中だけでなく、レースでさえも音楽を聞きながら走っています。トレーニングに不必要なものを取り除き、屋外でトレーニングしている時は、自分の足音や、鳥の鳴き声、その他の自然の音に耳を傾けましょう。自然の音を聞くのは、どこに住んでいるかで制限されるかもしれませんが、市街地を走っていても聞くに値する音はあるものです。

前にトレイルランをしていたら、山に来ているのにイヤホンつけてトレッキングしている人とすれ違って、なんだか悲しい気持ちになったよ。『自然の音に耳を傾けよう』って、本当にそう思う。まぁ、市街地だと車とか、電車の騒音ばかりなので、あまり聞いていて心地良い音ってほとんど無いけどね。

    
    Only take this information as you feel would be most beneficial to your training. Not every suggestions may be the best option for you to take. For example, you may be a minimalist runner in every aspect, but like to track your workouts using a GPS system and analyze the statistics. Rather than pointing out the areas of running where you are not taking a minimalist approach, this article is more about making you aware of how many areas of running the minimalist approach can be applied to.

    感じるままにこの情報を受け取る事が、あなたのトレーニングに最も利益があるでしょう。すべての提案を受け入れることがあなたにとって最善ではないかも知れません。例えば、あなたは、あらゆる面でミニマリストランナーかも知れませんが、GPSシステムを使ってトレーニングを追跡し、統計情報を解析するのが好きかも知れません。ミニマリストなアプローチを取っていない側面を指摘するのではなく、この記事がより多くのミニマリストアプローチを適用できるランニングの側面に気づくきっかけになれば幸いです。

いかがでしたか？ランニングにまつわる様々な面に関して、これだけ包括的にミニマリスト的アプローチが解説されているまとまった文章は見たことがない気がします。わたしもまだまだミニマリストランナーへの道に入ったばかりなので、捨てられるものは捨て、残していくものは残していくで、これからも試行錯誤していきたいと思います。
