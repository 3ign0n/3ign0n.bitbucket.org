---
layout: post
title: クリエイティブの可能性 春合宿 (3) 南三陸町のいま
category : "24_7"
tags : [creative, possibility, tohoku, volunteer]
---
{% include JB/setup %} 

<!-- 3ページ目 -->

### 待っていたのは死と生の世界


<a href="https://picasaweb.google.com/lh/photo/GNL5YuYMb_RY8egbE6sOQdMTjNZETYmyPJy0liipFm0?feat=embedwebsite"><img src="https://lh6.googleusercontent.com/-Z7h_BRlnZc8/T5AcHFz1VsI/AAAAAAAAAJg/d4-mIGMwIhE/s400/steel_skelton_govenment_disaster_prevention_building.jpg" height="300" width="400" /></a>


バスに揺られ、最初に向かったのは南三陸町。
高速道路を降りたあと、ぼんやりと窓の外を眺めていた。
ここはもう宮城県。最初に思ったのは、地震そのものの被害は外から見ている感じではあまりわからないんだな、と。
一年経ったからかもしれない。
だとしても、津波の方がずっと大きな被害をもたらしたという話は、
[阪神淡路大震災と東北関東大震災（東日本大震災）の比較](http://www.tamagoya.ne.jp/potechi/b/data/jishin.php)のようなサイトを見て、知識としてはあった。
ええ、頭でかちですとも。

バスが海岸に近づくにつれ、津波の爪痕が視界に迫ってくる。
TVやネットに転がる写真で何度も見た、あの光景が目の前に広がっていた。
いや、大部分は、重機によって瓦礫が端に追いやられ、ほぼ更地の状態になっていたので、
当時ほどの悲惨さはそこにはない。

口を開く者はほとんどいなかった。
皆黙って、目の前の光景に思いを巡らせている。
鉄骨だけになった防災庁舎。
賛否両論あるとしながらも、町長が南三陸のモニュメントとして残す意向を示したというニュースが話題になっていた。
[この防災庁舎では悲劇があった](http://memory.ever.jp/tsunami/higeki_bosai-tyosya.html)。
こういう話になると、『公人の義務』みたいな話が出てくるのがわたしはすごく嫌だ。
誰かの命を守るのに、誰かの命を犠牲にするような話に仕立てあげたり、
それに賛同したり、美化してはいけないと思う。
これを教訓にして各自がどうやって自分の身を守るのか、そういう事を考えるきっかけにならないと、と思う。

むき出しになった防災庁舎の周りには、建物はほとんど何も無い。
周囲にあったのは、木造の家だそうで、壊滅的に無くなっている。
そして、ふと地面に目をやると、ここに生活していた人たちの痕跡がいくつも。

<a href="https://picasaweb.google.com/lh/photo/elTmjIiMF0B-nxbWnmT0QtMTjNZETYmyPJy0liipFm0?feat=embedwebsite"><img src="https://lh5.googleusercontent.com/-iI0KUF8aO44/T5K1ZHg22UI/AAAAAAAAAKM/TO-oKHNqA94/s800/winnie_the_pooh_bear_soft_toy_on_the_ground.jpg" height="320" width="240" /></a>
<a href="https://picasaweb.google.com/lh/photo/mmlM0y8APUd_b_9LkkYOPtMTjNZETYmyPJy0liipFm0?feat=embedwebsite"><img src="https://lh3.googleusercontent.com/-ZVX-94B-8Vs/T5LIf1jB5UI/AAAAAAAAAKc/EXn_ARfLl6c/s800/a_spoon.jpg" height="320" width="240" /></a>
<a href="https://picasaweb.google.com/lh/photo/Co69_S4cdez0_xKBgMc4P9MTjNZETYmyPJy0liipFm0?feat=embedwebsite"><img src="https://lh6.googleusercontent.com/-nVnr_VmCzts/T5LIfRUK9OI/AAAAAAAAAKY/zOBMqv4t1nk/s800/a_bottle_of_coffee.jpg" height="320" width="240" /></a>

見渡すかぎりの荒廃した土地がそこにある。
一方で、震災なんて何もなかったかのように、優雅に川を泳ぐ鴨がいた。
川の水はとても澄んでいた。

<a href="https://picasaweb.google.com/lh/photo/lkuyOCxVrWSZt6D4bbP1kdMTjNZETYmyPJy0liipFm0?feat=embedwebsite"><img src="https://lh6.googleusercontent.com/-HaMNSV4xZ10/T5AcHhHfLbI/AAAAAAAAAJo/9b-ZeafM4oM/s800/wild_ducks.jpg" height="320" width="240" /></a>

その後、ボランティアセンターへ寄り、団体登録をした後、
ボランティアリーダーの指示の元、瓦礫撤去作業を行った。
現地の人で、写真を撮られることを快く思っていない人たちもいるので、
ビブスを着て活動している間は写真撮影はしない、という約束になっていた。
夜、この日の1日を振り返ったときに知ることになるが、この作業を辛いと感じた人は多かったようだ。
普段運動する習慣がない人には、肉体的にも疲労したようだが、
マラソンをやるわたしからすれば、なんてことはない運動量。
瓦礫を片付けていると、そこに人が生活をしていたのだと思わせるモノが、色々と出てくる。
そういうものを見て、精神的にも辛い気持ちになってしまった人もいたようだ。
そして、やってもやっても終わりが無いように思える瓦礫の山。
わたし自身は、とにかく目の前のモノに集中して作業できたので、別段、何の感情も抱かなかった。
これは、自分が今やっていることの意味を、わたしなりに理解していたから、というところもあると思う。

この瓦礫撤去の作業の意味は、実はボランティアリーダーの人がちゃんと話していた。
『その場の脇の道を通る人たちが、瓦礫を見て震災当時を思い出して辛いから』ということ。
復興に際して、この土地に何かを建てるためとか、そういう理由ではないのだ。
重機が入れるところは大体終わっているから、残っている重機の入れない狭い場所は人手でやるしかない。
ガラス、金属、コンクリート、燃える物、そういう風に分別することで、使える物はリサイクルするそうだ。
後から考えると、この分別の部分については、[こういう瓦礫を粉砕・分別してくれる機器](http://www.youtube.com/embed/uCAsUPy2I6o"
)を使ったほうがいいのではないかと思った。
50名程で半日かけて片付けを行って、片付いたのは、1ブロック四方( 戸建住戸にして、6-8戸程度)のみ。 
もちろん、細かい部分を見ていくと、完全には綺麗に拾いあげて分別できていないのだが、最初にこの瓦礫撤去の意味が説明されたとおり、
脇の道を通る人が見て、辛い思いをしない程度に綺麗になっていればいいのだ。
このへん、木を見て森を見ずになり、やきもきしてはいけない。

この日は、この後、陸前高田ボランティアセンター設立に携わった安田さんの話を聞いたり、
そして、この合宿の特色である『ダイアログ（対話）』というのを夜にやったのだが、
長くなったので、次回に分けて書く。


