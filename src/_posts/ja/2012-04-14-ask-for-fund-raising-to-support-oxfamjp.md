---
layout: post
title: ワンコインからでできる世界の問題解決への支援、はじめてみませんか？
category : "24_7"
tags : [international issues, charity, donation, run]
---
{% include JB/setup %}

<img src="http://www.ugokuugokasu.jp/imgs/topPhoto3.jpg" width="480" height="360" alt="ugokuugokasu"/>
©Photo: oxfam.jp

4/21に[Tokyo Yamathon](http://www.tokyo-yamathon.com/)というチャリティマラソンに出ます。
エントリー料7,000yenのうち、6,000yenは、[オックスファム・ジャパン](http://www.oxfam.jp/)へ寄付されます。
[オックスファム・ジャパン](http://www.oxfam.jp/)は、世界の貧困、医療、衛生、教育などの問題解決の為の支援や、緊急人道支援などを行っている特定非営利法人です。
また、昨年の東日本大震災発生後から、[継続的に被災地の支援を行ってくれています。](http://oxfam.jp/2012/03/311_1.html)

以下、オックスファムのミッションを引用します。

> オックスファムは、チャリティの団体ではありません。
> チェンジ（変化）の団体です。
> 貧困は、世界の共通課題です。現地だけではなく、貧困そのものを根本からなくすためには、先進国日本に住むわたしたちも貧困問題に対する認識を変える必要がある、とオックスファムは考えます。途上国にも、わたしたちの国内にも、豊かさの中に「貧困」があります。わたしたちの生活の中の習慣や、わたしたちの政府が取り決めた政策を、少しずつ改善していくだけで、その「貧困」をなくしていくことができます。そのために、オックスファムはキャンペーン活動を行い、先進国にいる人びと自身が変わることの大切さ、先進国の政策や国際社会の取り決めを変えていくことの大切さを伝えていきます。
> オックスファムの貧困削減への取り組みは、このように総合的に行われます。オックスファムの目指す、公正な世界を達成するために、オックスファムは常に着実な一歩一歩を踏みだし、確実な方法で達成していきます。

世界には様々な問題があります。
わたしたちはそうした問題に無知・無関心であったり、逆に、あまりに膨大な問題の山積みを前にして、なす術がないのではないかと無力感を味わったりします。

わたしは、今年、4/9〜3泊4日で、[東北へボランティアへ行ってきました。](http://3ign0n.bitbucket.org/24_7/2012/04/10/creative-possibility-spring-index)
今回、東日本大震災後、初めて東北へ足を運んだのですが、
自然の前ではあまりにも無力な人間、苦しみながらも少しずつ前へ進もうとしている人々、
そういうものを、実際に、肌で感じて、たとえ、ただ『伝える』だけであったり、
終わりの無い単調な作業の積み重ねに思えるようなことでも、いま自分にできることで実際に行動し、一歩を踏み出すことがとても大切だと思いました。

ボランティアというと身構えてしまう人がいますが、自分を犠牲にするのではなく、むしろ自身も楽しみながら、
やれることを『継続的に』やっていく、そういうことが本当に大切なのだと思いました。
もし、こうした問題により興味があるのであれば、オックスファム・ジャパンの『[あなたにできること](http://oxfam.jp/whatyoucan/)』のページを読んでみてください。

4/21に、東北へボランティアへ行ったメンバーと共に、
山手線を一周する[Tokyo Yamathon](http://www.tokyo-yamathon.com/)というチャリティマラソンに参加し、制限時間である12時間以内に完走を目指します。

山手線。電車で移動しても1周するのに1時間かかります。
あれを、人の足だけで、一周することを想像してみてください。
マラソンをやる人にとっては、フルマラソンと同じぐらいの距離といえばそれまでなのですが、
今回は、フルマラソンを走ったことがない人もチャレンジします！
また、交通規制を行うような大規模なマラソン大会ではないため、途中、遠回りしたり、陸橋を登ったり、
もちろんエイドステーションはありませんので、水分補給や、エネルギー補給の対策を全部自分でせねばならず、
通常のマラソン大会にはないような困難があります。
参加者は山手線各駅の29のチェックポイントを通らないといけません。
3〜4人のチームメンバー全員が全チェックポイントを通らないと、ゴールしたとは認められません。

わたしたちのチャレンジに賛同していただける方は、
[JustGivingで、オックスファム・ジャパンへの募金も募っています](http://justgiving.jp/c/8154?new=1)ので、
こちらもよろしくお願いします。

ワンコインからはじめられる、世界の問題解決への支援をはじめてみませんか？

<object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=9,0,0,0" width="150" height="280"><param name="allowScriptAccess" value="always" />
<param name="allowFullScreen" value="false" />
<param name="movie" value="http://justgiving.jp/widget/widget.swf?id=8154" />

<param name="quality" value="high" />
<param name="bgcolor" value="#ffffff" />
<embed src="http://justgiving.jp/widget/widget.swf?id=8154" quality="high" bgcolor="#ffffff" width="150" height="280" allowScriptAccess="always" allowFullScreen="false" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/go/getflashplayer" />
</object>
