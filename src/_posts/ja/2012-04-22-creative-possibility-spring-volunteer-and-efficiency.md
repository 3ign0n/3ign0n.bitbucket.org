---
layout: post
title: クリエイティブの可能性 春合宿 (4) ボランティアと効率の話
category : "24_7"
tags : [creative, possibility, tohoku, volunteer]
---

{% include JB/setup %} 


最初に結論を言う。
ボランティアにも効率化は必要だ。

昨年の記事だが、[大震災から３ヶ月、ボランティアに求められているものは？](http://abc1008.com/news/onair/110611.html)
は、このテーマを考える前に読むのに非常にオススメの記事。
でも、読んだ後に、『被災者のために』、かつ、『効率的に』何かをやることは可能では？と思った。

今回ボランティアに参加した際、何度も似たような表現が出てきた。
厳密な言葉は忘れたが『効率化を叫ぶ人もいますが、無理せず、怪我せずやってください。被災地の人たちは、こうしてみなさんが来て、少しでも手伝ってくれる事をとても喜んでくれていますから』
というような内容だったと思う。 
冒頭の『効率化』という言葉とそれ以降の話に何の因果関係もないな、と思った記憶がある。 
その時に、面と向かって『あんたの言ってることおかしいよ』と言うほどわたしは野暮ではなかったけど、
これに関してはハッキリとここで言っておかねばならないと思う。  
繰り返しになるが、ボランティアにも効率の追求は必要だ。 
無理をしない、怪我をしないのは、効率を追求する上では必要なこと。
誰かが怪我をすれば1人分の働き手が減るし、それどころか手当などをする人の分が更にマイナスとなるだろう。
全体的な効率を考えるのであれば、各自が無理をしないのは当然のことなのだ。
このへんは登山をやる人や、近代的なスポーツ科学を勉強している人なんかも当たり前に理解出来る話と思う。

[ボランティアなんぞに顔を出した反省文](http://anond.hatelabo.jp/touch/20110418205950) なんていう文章も昨年読んだ事を思い出した。
こういう話って実際にたくさんあったのだと思う。
闇雲に手を動かして、何かをやった気になるのはよくない。
1年経ったいま、南三陸を見た時も、陸前高田を見た時も、
今回一緒に行った人たちが津波の被害にあった場所を見て思ったことは『なにもない』だったと思う。
もちろん、そこにあった人々の生活や、家、建物、そういうものは津波によって破壊されてしまったのだけど、
震災直後はそこにあった瓦礫を重機を使い『効率的に』片付けたから、いまのような光景が広がっていたわけ。
効率を考慮せず、全部人の手でやっていたら、いまでも、震災当時とそれほど変わらない光景が残っていたかもしれない。
実際、冬にも南三陸町にボランティアに来た人は言っていた。
『冬に来たときは、瓦礫があって、こんなに先までは歩いてこれなかった』と。

今日、次のようなtweetを見かけた。

<blockquote class="twitter-tweet"><p>実利少なくても、頑張りましたっていうプロセスが重視される日本。“@<a href="https://twitter.com/Takuya20120219">Takuya20120219</a>: 駅前に体育会学生集団がズラリと並び、大声で募金を募っていた。全員で引越し屋のバイトをした方が絶対稼げると思いました。RT “@<a href="https://twitter.com/May_Roma">May_Roma</a>: RT @<a href="https://twitter.com/short_context">short_context</a>:</p>&mdash; Andy Kondo (@kdtwitting) <a href="https://twitter.com/kdtwitting/status/193975028289781760" data-datetime="2012-04-22T08:10:25+00:00">April 22, 2012</a></blockquote>
<script src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

この学生団体がどれだけ募金を集められたのか個別に報告を見てないから分からないけど、
確かに駅前の募金にお金を入れている人をわたしは見たことがない。
今自分がやっていることが、本当にそのままのやり方でいいのか、
まず、そういう問を発することが大切だということは言える。

今回東北に行って、その中でも最も活動的に動いている人たちの話を聞けたと思うけど、
みんな、どういう風に復興したらいいのか明確なビジョンがあるわけではなかった。
ただ、共通していたのは単に震災前に元に戻したいというだけではないということ。
復旧ではなく、復興なのだということ。
震災の傷を内包しながら、過去の精算作業と、未来の創造作業、その両方をどうやっていくか、みんな走りながら、考えながら日々過ごしていた。
だから、企業運営における全体最適化のような、『最も効率よく』ということはできないだろう。
でも、ちょっとした改善の積み重ねなら、やりながらでもできるでしょう？
被災者、いや、もうそういう呼び方は止めにして、復興を目指している人たち、ということにするが、
彼・彼女らの気持ちに寄り添いながらだって、
効率的に何かをやることってできるはずだとわたしは思ってる。
